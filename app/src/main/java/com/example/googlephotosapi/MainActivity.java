package com.example.googlephotosapi;

import android.accounts.Account;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private GoogleSignInAccount mGoogleSignInAccount = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choseAccount();
            }
        });
    }

    public void choseAccount() {
        try {
            Scope mScope = new Scope("https://www.googleapis.com/auth/photoslibrary");
            GoogleSignInOptions gso = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestScopes(mScope)
                    .requestEmail()
                    .build();
            GoogleApiClient mGoogleApiClient = new GoogleApiClient
                    .Builder(this)
                    .enableAutoManage(this, null)
                    .addScope(mScope)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, 1);
        } catch (Exception e) {
            Log.d("TAG",  e.getMessage());
        }
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                mGoogleSignInAccount = result.getSignInAccount();
            }
            if (mGoogleSignInAccount != null) {
                AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        String accessToken = null;
                        try {
                            accessToken = GoogleAuthUtil.getToken(
                                    MainActivity.this,
                                    new Account(mGoogleSignInAccount.getEmail(), GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE),
                                    "oauth2:https://www.googleapis.com/auth/photoslibrary");
                            Request request = new Request.Builder()
                                    .url("https://photoslibrary.googleapis.com/v1/mediaItems")
                                    .addHeader("Authorization", "Bearer " + accessToken)
                                    .build();
                            OkHttpClient client = new OkHttpClient();
                            Response response = null;
                            try {
                                response = client.newCall(request).execute();
                                String r = response.body().string();
                                Log.i("TAG", r);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException transientEx) {
                            Log.e("TAG", transientEx.toString());
                        } catch (UserRecoverableAuthException e) {
                            Log.e("TAG", e.toString());
                        } catch (GoogleAuthException authEx) {
                            Log.e("TAG", authEx.toString());
                        }
                        return accessToken;
                    }
                    @Override
                    protected void onPostExecute(String token) {
                        Log.i("TAG", "Access token retrieved:" + token);
                    }
                };
                task.execute();
            }
		}
	}
}
